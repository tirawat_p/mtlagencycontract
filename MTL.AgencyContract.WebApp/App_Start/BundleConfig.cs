﻿using System.Web;
using System.Web.Optimization;

namespace MTL.AgencyContract.WebApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/pluginLTE").Include(
                "~/Scripts/app.min.js",
                "~/Scripts/demo.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/pluginJS").Include(
                "~/Scripts/sweetalert.min.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/sweetalert.css",
                      "~/Content/AdminLTE.min.css",
                      "~/Content/skins/skin-mtl.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/font").Include(
                "~/Content/font-awesome.min.css",
                "~/Content/ionicons-2.0.1.min.css"));
        }
    }
}
