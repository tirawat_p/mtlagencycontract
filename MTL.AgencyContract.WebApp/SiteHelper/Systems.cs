﻿namespace MTL.AgencyContract.WebApp.SiteHelper
{
    public static class Systems
    {
        public static string GetAssemblyVerion()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            return assembly.GetName().Version.ToString();
        }
    }
}