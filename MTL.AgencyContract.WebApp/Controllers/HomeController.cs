﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MAAAS.Client;
using MAAAS.Client.MVC;

namespace MTL.AgencyContract.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly MTLAgentAuthentication _authorization = new MTLAgentAuthentication();
        private MTLAgentResources _agentResources;

        [MTLAuthorize]
        public ActionResult Index()
        {
            _authorization.CheckProviderIsAuthenticated();
            _agentResources = new MTLAgentResources(_authorization.GetToken().TokenAccess);
            var info = _agentResources.GetAgentDetail();
            return View();
        }

        public ActionResult IndexTop()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Login()
        {
            if (_authorization.IsAuthenticated)
            {
                return Redirect("~/home/index");
            }
            _authorization.ExternalAuthenticate();
            return View();
        }
    }
}